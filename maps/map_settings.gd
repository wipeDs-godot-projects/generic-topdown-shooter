extends Node2D

export (bool) var infinite
export (int) var waves
export (int) var zombies_base
export (float) var zombies_wave_multiplier

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func is_infinite():
	return self.infinite