<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tilesheet_topdownshooterkenney" tilewidth="64" tileheight="64" tilecount="540" columns="27">
 <image source="../../tilesheets/tilesheet_topdownshooterkenney.png" width="1728" height="1280"/>
 <tile id="26" probability="20"/>
 <tile id="53" probability="20"/>
 <tile id="80" probability="20"/>
 <tile id="107" probability="20"/>
 <tile id="134" probability="20"/>
 <tile id="161" probability="20"/>
 <tile id="162">
  <objectgroup draworder="index">
   <object id="1" x="54" y="54" width="9.25" height="10.125"/>
  </objectgroup>
 </tile>
 <tile id="163">
  <objectgroup draworder="index">
   <object id="6" x="0.125" y="53.875" width="9.75" height="10"/>
  </objectgroup>
 </tile>
 <tile id="166">
  <objectgroup draworder="index">
   <object id="1" x="-0.125" y="0" width="10.125" height="64"/>
  </objectgroup>
 </tile>
 <tile id="167">
  <objectgroup draworder="index">
   <object id="2" x="53.75" y="-0.125" width="10.375" height="64.25"/>
  </objectgroup>
 </tile>
 <tile id="188" probability="20"/>
 <tile id="189">
  <objectgroup draworder="index">
   <object id="1" x="54" y="-0.375" width="9.375" height="10.125"/>
  </objectgroup>
 </tile>
 <tile id="190">
  <objectgroup draworder="index">
   <object id="1" x="-0.25" y="0" width="10.25" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="193">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0.125" width="64" height="9.875"/>
  </objectgroup>
 </tile>
 <tile id="194">
  <objectgroup draworder="index">
   <object id="1" x="-0.125" y="54" width="64.125" height="10"/>
  </objectgroup>
 </tile>
 <tile id="215" probability="20"/>
</tileset>
