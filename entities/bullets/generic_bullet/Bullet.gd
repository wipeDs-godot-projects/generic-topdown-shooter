extends RigidBody2D

#var default_sprite = load("res://entities/bullets/sprites/bulletSilverSilver_outline.png")
var shot_by
var speed = 500
var damage = 50
var rot

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	rot = Vector2(sin(rotation), -cos(rotation))
	
	add_to_group(Utils.GROUP_BULLET)
	
	apply_impulse(Vector2(0,0), rot * speed)
	
	connect("body_entered", self, "_on_body_entered")
	
	#set_physics_process(true)
	pass

func set_speed(speed):
	self.speed = speed

func set_damage(damage):
	self.damage = damage

func _on_body_entered(body):
	#if (col != null && col.collider.is_in_group(Utils.GROUP_ZOMBIE)):
	#print("collided: " + str(body))
	if body.has_method("_on_hit"):
		body._on_hit(damage, shot_by)
	#set_physics_process(false)
	queue_free()
	pass