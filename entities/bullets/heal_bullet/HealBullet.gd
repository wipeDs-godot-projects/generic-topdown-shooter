extends "res://entities/bullets/generic_bullet/Bullet.gd"

func _ready():
	pass


func _on_body_entered(body):
	#if (col != null && col.collider.is_in_group(Utils.GROUP_ZOMBIE)):
	#print("collided: " + str(body))
	if body.has_method("_on_heal"):
		body._on_heal(damage, shot_by)
	#set_physics_process(false)
	queue_free()
	pass