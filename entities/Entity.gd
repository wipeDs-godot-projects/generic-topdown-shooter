extends KinematicBody2D

signal health_changed
signal health_max_changed
signal health_increased
signal died
signal shieldpack
signal pickup

func _ready():
	add_to_group(Utils.GROUP_ENTITY)
	connect("died", self, "_on_death")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_death():
	if !self.is_in_group(Utils.GROUP_PLAYER):
		queue_free()
	else:
		#die()
		print("wot")
	pass
