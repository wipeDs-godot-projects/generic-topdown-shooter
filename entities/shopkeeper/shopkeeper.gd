extends StaticBody2D


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("shop_area").connect("body_entered", self, "_on_shape_enter")
	get_node("shop_area").connect("body_exited", self, "_on_shape_exit")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_shape_enter(other):
	if other.is_in_group(Utils.GROUP_PLAYER):
		other.emit_signal("shop_entered", self)

func _on_shape_exit(other):
	print("Exited: " + str(other))
	if other.is_in_group(Utils.GROUP_PLAYER):
		other.emit_signal("shop_exited", self)