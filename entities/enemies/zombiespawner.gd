extends Node

export(int) var player_radius = 1
export(int) var max_amount_zombies = 5
export(float) var spawn_rate = 10
export(float) var chance_to_drop_ammo = 0.25
export(float) var chance_to_drop_health = 0.25
export(float) var chance_to_drop_money = 0.7

var zombie_prefab = load("res://entities/enemies/Zombie_Generic.tscn")



var bounds;

func _ready():
	set_process(true)
	pass

func set_bounds():
	bounds = Utils.get_main_node().map_region
	pass
	
func _process(delta):
	if get_children().size() < max_amount_zombies:
		var new_zombie = zombie_prefab.instance()
		var dropping = generate_drop()
		new_zombie.drop = dropping
		new_zombie.position = generate_spawn_position()
		add_child(new_zombie)
	pass

func generate_drop():
	var drops_ammo = randf() > (1 - chance_to_drop_ammo)
	var drops_health = false
	if !drops_ammo:
		if randf() > (1 - chance_to_drop_money):
			return Enums.Drops.MONEY
		else:
			if randf() > (1 - chance_to_drop_health):
				return Enums.Drops.HEALTH
			else:
				return Enums.Drops.NONE
	else:
		return Enums.Drops.AMMO
	pass

func generate_spawn_position():
	var rand_pos = Vector2(rand_range(Utils.get_main_node().TILESIZE, bounds.x - Utils.get_main_node().TILESIZE),
						   rand_range(Utils.get_main_node().TILESIZE, bounds.y - Utils.get_main_node().TILESIZE))
	var is_far_enough = false
	while(!is_far_enough):
		for p in get_tree().get_nodes_in_group(Utils.GROUP_PLAYER):
			if rand_pos.distance_to(p.position) < player_radius * Utils.get_main_node().TILESIZE && !is_far_enough:
				rand_pos = generate_spawn_position()
			else:
				is_far_enough = true
	
	return rand_pos