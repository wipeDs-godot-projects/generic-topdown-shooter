extends "res://entities/Entity.gd"

onready var health = MAX_HEALTH
export(int) var MAX_HEALTH = 100
export(int) var damage = 10
export(int) var speed = 20
var target
var drop

onready var timer = get_node("attack_timer")

#onready var DROP_AMMO = load("res://entities/pickups/[..].gd") # not implemented yet
onready var DROP_HEALTH = load("res://entities/pickups/Health/Health.tscn")
onready var DROP_MONEY = load("res://entities/pickups/Money/Money.tscn")

func _ready():
	connect("died", self, "_on_death_drop")
	#print (speed)
	add_to_group(Utils.GROUP_ZOMBIE)
	select_target()
	set_process(true)
	set_physics_process(true)
	pass

func _physics_process(delta):
	look_at(target.position)
	var direction = target.position - self.global_position
	var col = move_and_collide(direction.normalized() * speed * delta)
	if col != null and col.collider.is_in_group(Utils.GROUP_PLAYER):
		_on_character_hit(col.collider)
	pass

func _on_character_hit(body):
	if timer.is_stopped() and body.has_method("_on_hit"):
		body._on_hit(damage, self)
		timer.start()
	pass

func select_target():
	var possible_targets = get_tree().get_nodes_in_group(Utils.GROUP_PLAYER)
	#print(possible_targets)
	if (possible_targets.size() == 0):
		return
	var shortest_distance = INF
	
	for e in possible_targets:
		var e_distance = self.global_position.distance_to(e.global_position)
		if e_distance < shortest_distance:
			shortest_distance = e_distance
			target = e
	
	target.connect("died", self, "select_new_target")
	set_physics_process(true)
	pass

func select_new_target():
	set_physics_process(false)
	target.disconnect("died", self, "select_new_target")
	target = null
	select_target()
	pass
	
func _on_hit(damage, entity):
	#print (str(entity) + " hit you for " + str(damage) + " damage.")
	self.health -= damage
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("died")
		if entity.has_method("increase_zombie_killcount"):
			#print("increased zombie killcount")
			entity.increase_zombie_killcount()
		#print ("dead")
	pass

func _on_death_drop():
	var droppy
	match drop:
		Enums.Drops.AMMO:
			print("Dropping ammo")
		Enums.Drops.HEALTH:
			print("Dropping health")
			droppy = DROP_HEALTH.instance()
		Enums.Drops.MONEY:
			print("Dropping Money")
			droppy = DROP_MONEY.instance()
	if droppy != null:
		droppy.position = self.position
		Utils.get_main_node().get_node("pickups").add_child(droppy)
	pass