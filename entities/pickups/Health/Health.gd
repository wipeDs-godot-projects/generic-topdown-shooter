extends Node2D

export(int) var heal_amount = 25

func _ready():
	pass


func _on_pickup_area_body_entered(body):
	if body.is_in_group(Utils.GROUP_PLAYER):
		body.emit_signal("health_increased", heal_amount)
		#print ("pickupzzz")
		queue_free()
	pass # replace with function body
