extends Node2D

var money_amount

const MONEY_MAX = 100
const MONEY_MIN =  15

func _ready():
	money_amount = int(rand_range(MONEY_MIN, MONEY_MAX))
	pass


func _on_pickup_area_body_entered(body):
	if body.is_in_group(Utils.GROUP_PLAYER):
		body.increase_money(money_amount)
		print ("money pickup")
		queue_free()
	pass # replace with function body
