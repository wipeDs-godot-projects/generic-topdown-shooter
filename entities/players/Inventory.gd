extends Node

var items: Dictionary
var current_equipped = "main_1"
var money = 50000

var last_main = "main_1"
var last_tool = "tool_1"

signal inventory_changed(slot, gun)

func _ready():
	items["main_1"] = $inventory_guns/rifle
	items["main_2"] = $inventory_guns/awp
	items["sidearm"] = $inventory_guns/healing_pistol
	items["throwable"] = $inventory_throwables/grenade
	items["tool_1"] = $inventory_tools/healing_syringe
	items["tool_2"] = null
	
	initial_signal_emits()
	pass # Replace with function body.

func get_current_gun():
	return items[current_equipped]

func get_switched_slot(gun_slot):
	if gun_slot == "main":
		match current_equipped:
			"main_1":
				last_main = "main_2"
				return "main_2"
			"main_2":
				last_main = "main_1"
				return "main_1"
			_:
				return last_main
	elif gun_slot == "melee":			#there's no melee yet
		return "sidearm"
	elif gun_slot == "tool":
		match current_equipped:
			"tool_1":
				last_tool = "tool_2"
				return "tool_2"
			"tool_2":
				last_tool = "tool_1"
				return "tool_1"
			_:
				return "tool_1"
	return gun_slot

func switch_gun(gun_slot):
	var switched_slot = get_switched_slot(gun_slot)
	if items[switched_slot] != null:
		if !get_current_gun().reload_timer.is_stopped():
			get_current_gun().reload_abort()
		get_current_gun().hide()
		current_equipped = switched_slot
		get_current_gun().show()
		get_parent().emit_signal("weapon_changed", items[current_equipped])
	pass

func initial_signal_emits():
	for item in items:
		emit_signal("inventory_changed", item, items[item])

func change_main_1(gun):
	items["main_1"] = gun
	emit_signal("inventory_changed", "main_1", gun)

func change_main_2(gun):
	items["main_2"] = gun
	emit_signal("inventory_changed", "main_2", gun)

func change_sidearm(gun):
	items["sidearm"] = gun
	emit_signal("inventory_changed", "sidearm", gun)

func change_throwable(throwable):
	items["throwable"] = throwable
	emit_signal("inventory_changed", "throwable", throwable)

func change_tool_1(tool_1):
	items["tool_1"] = tool_1
	emit_signal("inventory_changed", "tool_1", tool_1)

func change_tool_2(tool_2):
	items["tool_2"] = tool_2
	emit_signal("inventory_changed", "tool_2", tool_2)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
