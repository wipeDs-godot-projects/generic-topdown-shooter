extends Node2D

var ui_name
var tool_owner
var price = -1
var timer = Timer.new()
var reload_timer = Timer.new()
var slot_type = Enums.WEAPON_SLOT_TYPE.TOOL

func _ready():
	tool_owner = get_parent().get_parent().get_parent()
	pass 

func get_bullets():
	return 0

func get_bullets_max():
	return 0

func get_ammo_reservoir():
	return 0

func get_ammo_reservoir_max():
	return 0

func get_bullet_price():
	return 0

func get_price():
	return price