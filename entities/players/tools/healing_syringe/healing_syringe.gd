extends "res://entities/players/tools/generic_tool.gd"

const UI_NAME = "HEALING SYRINGE"
const HEAL_AMOUNT = 40
const PRICE = 5000



# Called when the node enters the scene tree for the first time.
func _ready():
	ui_name = UI_NAME
	price = PRICE
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func shoot():
	# heal the player, animation
	tool_owner.increase_health(HEAL_AMOUNT)
	pass
