extends Node2D

var ui_name = "THROWABLE"
var grenades = 1
var grenades_reservoir = 2
var grenades_reservoir_max = 5
var grenade_price = 50
var timer = Timer.new()
var reload_timer = Timer.new()
var slot_type = Enums.WEAPON_SLOT_TYPE.THROWABLE

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func throw():
	pass

func add_to_ammo_reservoir(difference):
	grenades_reservoir += difference

func get_bullets():
	return grenades

func get_bullets_max():
	return 1

func get_bullet_price():
	return grenade_price

func get_ammo_reservoir():
	return grenades_reservoir

func get_price():
	return grenade_price

func get_ammo_reservoir_max():
	return grenades_reservoir_max