extends "res://entities/Entity.gd"

export(int) var MAX_HEALTH = 100
export(int) var MAX_SHIELD = 100
export(int) var SHIELD_DAMAGE_FACTOR = 2
export(int) var speed = 160

var zombie_killcount = 0
var current_shop = null
var just_left_shop = false
var camera

onready var gun_inventory = [$inventory/inventory_guns/awp, $inventory/inventory_guns/pistol, null,  null, $inventory/inventory_tools/healing_syringe]

onready var current_gun = $inventory/inventory_guns/pistol
onready var health = 50
onready var shield = 37
onready var money = $inventory.money

signal money_changed
signal shield_changed
signal shield_max_changed
signal zombie_killed
signal weapon_changed
signal weapon_ammo_count_changed
signal weapon_reload_start
signal weapon_reload_abort
signal weapon_reload_boost
signal weapon_reload_finished
signal shop_entered
signal shop_exited

func _ready():
	set_process_input(true)
	set_physics_process(true)
	add_to_group(Utils.GROUP_PLAYER)
	
	connect("health_increased", self, "increase_health")
	connect("shop_entered", self, "_on_shop_entered")
	connect("shop_exited", self, "_on_shop_exited")
	emit_signal("health_changed", self.health)
	emit_signal("shield_changed", self.shield)
	$inventory.switch_gun("main_1")
	camera = Utils.get_main_node().get_node("Camera")
	
	pass

func _process(delta):
	if is_processing_input():
		self.look_at(get_global_mouse_position())
		
		if Input.is_action_just_pressed("game_zoom_in"): camera.zoom /= 2;
		elif Input.is_action_just_pressed("game_zoom_out"): camera.zoom *= 2;
		
		if Input.is_action_pressed("p1_fire"):
			if $inventory.get_current_gun().has_method("shoot"):
				$inventory.get_current_gun().shoot()
			elif $inventory.get_current_gun().has_method("throw"):
				$inventory.get_current_gun().throw()
		if Input.is_action_pressed("p1_fire_alternate"):
			if $inventory.get_current_gun().has_method("alternate_shoot"):
				$inventory.get_current_gun().alternate_shoot()
				
		if Input.is_action_pressed("p1_fire_special"):
			if $inventory.get_current_gun().has_method("special_shoot"):
				$inventory.get_current_gun().special_shoot()
		
		if Input.is_action_just_pressed("p1_reload"):
			$inventory.get_current_gun().reload()
		
		if Input.is_action_just_pressed("p1_switch_slot_1"):
			$inventory.switch_gun("main")
		if Input.is_action_just_pressed("p1_switch_slot_2"):
			$inventory.switch_gun("sidearm")
		if Input.is_action_just_pressed("p1_switch_slot_3"):
			$inventory.switch_gun("melee")
		if Input.is_action_just_pressed("p1_switch_slot_4"):
			$inventory.switch_gun("tool")
		if Input.is_action_just_pressed("p1_switch_slot_5"):
			$inventory.switch_gun("throwable")
		
		if current_shop != null && !just_left_shop && Input.is_action_just_pressed("enter_shop"):
			emit_signal("money_changed", self.money)
			Utils.get_main_node().find_node("ui_shop").show()
			just_left_shop = true
			set_process_input(false)
		if Input.is_action_just_released("enter_shop"):
			just_left_shop = false
		debug_input(delta)
	pass

func debug_input(delta):
	if Input.is_key_pressed(KEY_F1):
		print("yey")
	pass

func _physics_process(delta):
	if is_processing_input():
		handle_movement(delta)

func _on_shop_entered(shop):
	self.current_shop = shop

func _on_shop_exited(shop):
	self.current_shop = null

func _on_hit(damage, entity):
	#print (str(entity) + " hit you for " + str(damage) + " damage.")
	var damage_left = damage
	if shield > 0:
		shield -= damage * SHIELD_DAMAGE_FACTOR
		emit_signal("shield_changed", shield)
		if shield < 0:
			damage_left = abs(shield / 2)
		else:
			damage_left = 0
	self.health -= damage_left
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("died")
		#print ("dead")
	pass

func increase_health(hp):
	print_debug("health increased by " + str(hp))
	self.health += hp
	if self.health >= MAX_HEALTH:
		self.health = MAX_HEALTH
	emit_signal("health_changed", self.health)

func _on_repair_shield(price):
	var amount
	var actual_price
	if price < money:
		amount = price / Utils.get_main_node().SHIELD_PRICE
		actual_price = price
	else:
		amount = floor(money / Utils.get_main_node().SHIELD_PRICE)
		actual_price = amount * Utils.get_main_node().SHIELD_PRICE
	self.shield = self.shield + amount
	decrease_money(actual_price)
	print_debug("repaired shield, amount:" + str(amount) + ", price:" + str(actual_price))
	emit_signal("shield_changed", shield)

func increase_zombie_killcount():
	zombie_killcount += 1
	emit_signal("zombie_killed", zombie_killcount)
	pass

func increase_money(amount):
	money += amount
	
	# hardcap of money to stay within ui boundaries
	if money > 99999:
		money = 99999
	emit_signal("money_changed", money)
	pass

func decrease_money(amount):
	money -= amount
	if money < 0:
		print_debug("MONEY WENT BELOW ZERO")
	emit_signal("money_changed", money)

func handle_movement(delta):	
	var motion = Vector2()
	
	if Input.is_action_pressed("p1_walk_up"):
		motion += Vector2(0, -1)
	if Input.is_action_pressed("p1_walk_down"):
		motion += Vector2(0, 1)
	if Input.is_action_pressed("p1_walk_left"):
		motion += Vector2(-1, 0)
	if Input.is_action_pressed("p1_walk_right"):
		motion += Vector2(1, 0)
	
	motion = motion.normalized() * speed

	if motion != Vector2(0, 0):
		camera.screenshake()

	self.move_and_slide(motion)
	pass

func die():
	#print("test, you're ded lol")
	set_process(false)
	set_process_input(false)
	set_physics_process(false)