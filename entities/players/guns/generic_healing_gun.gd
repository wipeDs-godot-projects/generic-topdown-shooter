extends "res://entities/players/guns/generic_gun.gd"

var heal_cooldown
var heal_timer
var heal_amount
var heal_bullet = load("res://entities/bullets/heal_bullet/HealBullet.tscn")

func _ready():
	heal_timer = Timer.new()
	heal_timer.one_shot = true
	add_child(heal_timer)
	pass


func special_shoot():
	if heal_timer.is_stopped():
		var created_bullet = create_bullet(heal_bullet, velocity, heal_amount)
		heal_timer.start()
	pass