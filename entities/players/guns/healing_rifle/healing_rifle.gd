extends "res://entities/players/guns/generic_healing_gun.gd"

const SHOOT_COOLDOWN = 0.05
const RELOAD_TIME = 2
const BULLETS_MAX = 30
const UI_NAME = "HEALING RIFLE"
const PRICE = 1000
const AMMO_RESERVOIR_MAX = 300
const BULLET_PRICE = 15

## Healing Gun
const HEAL_COOLDOWN = 5
const HEAL_AMOUNT = 25

func _ready():
	slot_type = Enums.WEAPON_SLOT_TYPE.RIFLE
	ui_name =  UI_NAME
	bullets_max = BULLETS_MAX
	bullets = BULLETS_MAX
	reload_time = RELOAD_TIME
	price = PRICE
	ammo_reservoir = BULLETS_MAX * 3
	ammo_reservoir_max = AMMO_RESERVOIR_MAX
	bullet_price = BULLET_PRICE
	heal_amount = HEAL_AMOUNT
	
	timer.one_shot = true
	timer.wait_time = SHOOT_COOLDOWN
	heal_timer.wait_time = HEAL_COOLDOWN
	reload_timer.wait_time = RELOAD_TIME
	pass
