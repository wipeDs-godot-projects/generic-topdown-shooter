extends Node2D

export(int) var velocity = 500
export(int) var damage = 50

var slot_type
var gun_owner
var bullets_max setget , get_bullets_max
var bullets setget , get_bullets
var bullet_price setget ,get_bullet_price
var ammo_reservoir setget set_ammo_reservoir, get_ammo_reservoir
var ammo_reservoir_max setget ,get_ammo_reservoir_max
var ui_name
var price setget , get_price
var bullet = load("res://entities/bullets/generic_bullet/Bullet.tscn")
var timer = Timer.new()
var reload_timer = Timer.new()
var reload_time = -1

const GUN_FIST    = "_stand.png"
const GUN_PISTOL  = "_pistol.png"
const GUN_MACHINE = "_machine.png"

func _ready():
	gun_owner = get_parent().get_parent().get_parent()
	add_child(timer)
	
	## Reload Timer settings
	reload_timer.one_shot = true
	add_child(reload_timer)
	pass

func create_bullet(bullet, velocity, damage):
	var actual_bullet = bullet.instance()
	actual_bullet.position = get_node("bullet_pos").global_position
	actual_bullet.rotation = deg2rad(self.global_rotation_degrees + 90)
	actual_bullet.shot_by = self
	actual_bullet.set_speed(velocity)
	actual_bullet.set_damage(damage)
	Utils.get_main_node().add_child(actual_bullet)
	return actual_bullet

func shoot():
	if bullets > 0 && timer.is_stopped() && reload_timer.is_stopped():
		create_bullet(bullet, velocity, damage)
		bullets -= 1
		gun_owner.emit_signal("weapon_ammo_count_changed", bullets, get_ammo_reservoir())
		timer.start()

func reload():
	if bullets < bullets_max && reload_timer.is_stopped():
		gun_owner.emit_signal("weapon_reload_start", reload_time)
		reload_timer.connect("timeout", self, "reload_finished")
		reload_timer.start()

func reload_abort():
	reload_timer.stop()
	gun_owner.emit_signal("weapon_reload_abort")

func reload_finished():
	var ammo_difference = bullets_max - bullets
	if ammo_reservoir - ammo_difference < 0:
		bullets = ammo_reservoir
		ammo_reservoir = 0
	else:
		bullets = bullets_max
		ammo_reservoir = ammo_reservoir - ammo_difference
	gun_owner.emit_signal("weapon_ammo_count_changed", bullets, get_ammo_reservoir())
	gun_owner.emit_signal("weapon_reload_finished")

func increase_zombie_killcount():
	gun_owner.increase_zombie_killcount()
	pass

func get_bullets_max():
	return bullets_max

func get_bullets():
	return bullets

func get_price():
	return price

func set_ammo_reservoir(amount):
	ammo_reservoir = amount
	gun_owner.emit_signal("weapon_ammo_count_changed", get_bullets(), get_ammo_reservoir())

func add_to_ammo_reservoir(amount):
	if amount < 0:
		print_debug("ADDED NEGATIVE AMOUNT OF AMMO TO RESERVOIR")
	ammo_reservoir += amount
	gun_owner.emit_signal("weapon_ammo_count_changed", get_bullets(), get_ammo_reservoir())
func remove_from_ammo_reservoir(amount):
	if amount < 0:
		print_debug("REMOVED NEGATIVE AMOUNT OF AMMO FROM RESERVOIR")
	ammo_reservoir -= amount
	gun_owner.emit_signal("weapon_ammo_count_changed", get_bullets(), get_ammo_reservoir())

func get_ammo_reservoir():
	return ammo_reservoir

func get_ammo_reservoir_max():
	return ammo_reservoir_max

func get_bullet_price():
	return bullet_price
