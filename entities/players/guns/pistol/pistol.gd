extends "res://entities/players/guns/generic_gun.gd"

const SHOOT_COOLDOWN = 0.25
const RELOAD_TIME = 1.5
const BULLETS_MAX = 12
const UI_NAME = "PISTOL"
const PRICE = 250
const AMMO_RESERVOIR_MAX = 120
const BULLET_PRICE = 5


func _ready():
	slot_type = Enums.WEAPON_SLOT_TYPE.SIDEARM
	ui_name = UI_NAME
	bullets_max = BULLETS_MAX
	bullets = bullets_max
	reload_time = RELOAD_TIME
	price = PRICE
	ammo_reservoir = BULLETS_MAX * 3
	ammo_reservoir_max = AMMO_RESERVOIR_MAX
	bullet_price = BULLET_PRICE
	
	timer.one_shot = true
	timer.wait_time = SHOOT_COOLDOWN
	reload_timer.wait_time = RELOAD_TIME
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass