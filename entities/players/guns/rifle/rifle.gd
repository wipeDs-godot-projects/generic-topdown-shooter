extends "res://entities/players/guns/generic_gun.gd"

const SHOOT_COOLDOWN = 0.05
const RELOAD_TIME = 2
const BULLETS_MAX = 30
const UI_NAME = "RIFLE"
const PRICE = 1000
const AMMO_RESERVOIR_MAX = 300
const BULLET_PRICE = 15

func _ready():
	slot_type = Enums.WEAPON_SLOT_TYPE.RIFLE
	ui_name =  UI_NAME
	bullets_max = BULLETS_MAX
	bullets = BULLETS_MAX
	reload_time = RELOAD_TIME
	price = PRICE
	ammo_reservoir = BULLETS_MAX * 3
	ammo_reservoir_max = AMMO_RESERVOIR_MAX
	bullet_price = BULLET_PRICE
	
	timer.one_shot = true
	timer.wait_time = SHOOT_COOLDOWN
	reload_timer.wait_time = RELOAD_TIME
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass