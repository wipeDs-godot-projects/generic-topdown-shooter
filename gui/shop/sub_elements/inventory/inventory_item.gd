extends Control

export(String) var slot_name

var character
var cost_refill = 0
var current_gun
var sell_value

func _ready():
	character = Utils.get_main_node().find_node("Character")
	character.connect("money_changed", self, "_on_money_changed")
	character.get_node("inventory").connect("inventory_changed", self, "_on_inventory_changed")
	character.get_node("inventory").initial_signal_emits()
	
	# Get the shop-node to connect it's signal
	get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().connect("visibility_changed", self, "_on_shop_open", [current_gun])
	pass # Replace with function body.

func _on_shop_open(gun):
	set_ammo_amounts(gun)
	set_refill_costs(gun)

func _on_inventory_changed(slot, gun):
	if slot == slot_name:
		set_gun_label(gun)
		set_ammo_amounts(gun)
		set_refill_costs(gun)
		set_sell_value(gun)
		current_gun = gun

func _on_money_changed(money):
	set_refill_costs(current_gun)
	pass

func set_gun_label(label):
	if label == null:
		$PanelContainer/HBoxContainer/vbox_desc/item_label.text = "NONE"
	else:
		$PanelContainer/HBoxContainer/vbox_desc/item_label.text = label.ui_name

func set_ammo_amounts(gun):
	if gun == null:
		$PanelContainer/HBoxContainer/vbox_ammo/ammo_cur.text = "0/"
		$PanelContainer/HBoxContainer/vbox_ammo/ammo_max.text = "0"
	else:
		$PanelContainer/HBoxContainer/vbox_ammo/ammo_cur.text = str(gun.get_bullets() + gun.get_ammo_reservoir()) + "/"
		$PanelContainer/HBoxContainer/vbox_ammo/ammo_max.text = str(gun.get_ammo_reservoir_max() + gun.get_bullets_max())

func set_refill_costs(gun):
	if gun == null:
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_button.disabled = true
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_cost.text = "0€"
	elif character.money < gun.get_bullet_price():
		var missing_ammo = (gun.get_ammo_reservoir_max() + gun.get_bullets_max()) - (gun.get_bullets() + gun.get_ammo_reservoir())
		cost_refill = missing_ammo * gun.get_bullet_price()
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_cost.text = str(cost_refill) + "€"
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_button.disabled = true
	else:
		var missing_ammo = (gun.get_ammo_reservoir_max() + gun.get_bullets_max()) - (gun.get_bullets() + gun.get_ammo_reservoir())
		cost_refill = missing_ammo * gun.get_bullet_price()
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_cost.text = str(cost_refill) + "€"
		$PanelContainer/HBoxContainer/vbox_refill/item_refill_button.disabled = false
	pass

func set_sell_value(gun):
	if gun == null:
		$PanelContainer/HBoxContainer/vbox_sell/item_sell_button.disabled = true
		$PanelContainer/HBoxContainer/vbox_sell/item_sell_cost.text = "0€"
	else:
		sell_value = ceil(gun.get_price() * Utils.get_main_node().RESELL_FACTOR)
		$PanelContainer/HBoxContainer/vbox_sell/item_sell_cost.text = str(sell_value) + "€"
		$PanelContainer/HBoxContainer/vbox_sell/item_sell_button.disabled = false