extends Control
var cost
var character
var gun
var slot_name
var replacement_popup

func _ready():
	character = Utils.get_main_node().find_node("Character")
	character.connect("money_changed", self, "_on_money_changed")
	replacement_popup = get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().find_node("replace_popup")
	$PanelContainer/HBoxContainer/item_buy_button.connect("pressed", self, "_on_pressed")
	pass # Replace with function body.

func _on_pressed():
	replacement_popup.setup(gun, cost, character)
	replacement_popup.popup()

func set_icon(icon):
	$PanelContainer/HBoxContainer/item_icon.texture = icon

func set_label(label):
	$PanelContainer/HBoxContainer/item_label.text = label

func set_slot(slot):
	self.slot_name = slot

func set_gun(gun):
	self.gun = gun

func set_cost(cost):
	self.cost = cost
	$PanelContainer/HBoxContainer/item_cost.text = str(cost) + "€"

func set_bought(bought):
	$PanelContainer/HBoxContainer/item_buy_button.disabled = bought
	if bought:
		$PanelContainer/HBoxContainer/item_buy_button.text = "BOUGHT"
	else:
		$PanelContainer/HBoxContainer/item_buy_button.text = "SELL"

func _on_money_changed(money):
	if cost > money:
		$PanelContainer/HBoxContainer/item_buy_button.disabled = true
	else:
		$PanelContainer/HBoxContainer/item_buy_button.disabled = false
