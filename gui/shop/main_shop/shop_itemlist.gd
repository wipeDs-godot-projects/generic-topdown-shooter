extends ScrollContainer

onready var ITEM_SCENE = load("res://gui/shop/sub_elements/equipment/equipment_item.tscn")

onready var PLACEHOLDER_ICON: Texture = load("res://icon.png")
var total_count = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	var character = Utils.get_main_node().find_node("Character").get_node("inventory")
	var character_guns = character.get_node("inventory_guns")
	var character_throwables = character.get_node("inventory_throwables")
	var character_tools = character.get_node("inventory_tools")
	
	if $VBoxContainer.get_child_count() > 0:
		for child in $VBoxContainer.get_children():
			child.queue_free()
	parse_items(character_guns)
	parse_items(character_throwables)
	parse_items(character_tools)
		
	#Empty child cause the last one isn't displayed when scrollingfor some reason
	if total_count > 12:
		$VBoxContainer.add_child(ITEM_SCENE.instance())
	pass

func parse_items(parent):
	for i in parent.get_children():
		var current_item = ITEM_SCENE.instance()
		current_item.set_icon(PLACEHOLDER_ICON)
		current_item.set_label(i.ui_name)
		current_item.set_cost(i.get_price())
		current_item.set_gun(i)
		$VBoxContainer.add_child(current_item)
		total_count += 1
