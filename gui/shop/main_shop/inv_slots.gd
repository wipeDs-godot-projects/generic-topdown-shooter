extends VBoxContainer

# Constants for the names of each slot
const PRIMARY_GUN_1 = "PRIMARY GUN 1"
const PRIMARY_GUN_2 = "PRIMARY GUN 2"
const SECONDARY_GUN = "SECONDARY GUN"
const THROWABLE = "THROWABLE"
const TOOL_1 = "TOOL 1"
const TOOL_2 = "TOOL 2"


func _ready():
	# Renaming the placeholders to their specific values
	$inv_main_1/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = PRIMARY_GUN_1
	$inv_main_2/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = PRIMARY_GUN_2
	$inv_secondary/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = SECONDARY_GUN
	$inv_throwable/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = THROWABLE
	$inv_tool_1/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = TOOL_1
	$inv_tool_2/PanelContainer/HBoxContainer/vbox_desc/slot_label.text = TOOL_2
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
