extends ItemList

var items_array = ["Main 1: Healing Rifle", "Main 2: Healing SMG", "Sidearm: Deagle", "Tool 1: XY", "Tool 2: XY"]

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(items_array.size()):
		add_item(items_array[i])
	select(0,true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
