extends Control

onready var healthbar = get_node("healthpart")
onready var character = get_parent()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	character.connect("health_changed", self, "set_health")
	set_as_toplevel(true)
	set_process(true)
	pass

func _process(delta):
	self.rect_position = get_parent().position + Vector2(-24, -32)
	pass

func set_health(hp):
	var scaledelta = healthbar.rect_scale.x - (hp / 100.0)
	healthbar.rect_scale.x -= scaledelta
	pass
