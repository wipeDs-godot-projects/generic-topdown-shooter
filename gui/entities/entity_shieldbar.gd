extends Control

onready var shieldbar = get_node("shieldpart")
onready var character = get_parent()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	character.connect("shield_changed", self, "set_shield")
	set_as_toplevel(true)
	set_process(true)
	pass

func _process(delta):
	self.rect_position = get_parent().position + Vector2(-24, -32)
	pass

func set_shield(hp):
	var scaledelta = shieldbar.rect_scale.x - (hp / 100.0)
	shieldbar.rect_scale.x -= scaledelta
	pass
