extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(NodePath) var characterPath
export(NodePath) var cameraPath

var characterNode
var character_pos
var inventoryNode
var cameraNode
var ftext

func _ready():
	ftext  = "====== DEBUG ======\n"
	ftext += "Pos: ({posx}|{posy})\n"
	ftext += "Zoom: {zoom}x\n"
	ftext += "Money: {money}\n"
	ftext += "===== HOTKEYS =====\n"
	ftext += "F1/F2: Inc/Dec Health\n"
	ftext += "F3/F4: Inc/Dec Shield\n"
	ftext += "F5: Inc Money by 10k\n"
	ftext += "F6: Ammo for cur Wep\n"
	ftext += "===== Inventory =====\n"
	ftext += "Main1: {main_1}\n"
	ftext += "Main2: {main_2}\n"
	ftext += "Sec: {sidearm}\n"
	ftext += "Throw: {throwable}\n"
	ftext += "Tool1: {tool_1}\n"
	ftext += "Tool2: {tool_2}\n"
	
	characterNode = get_node(characterPath)
	inventoryNode = characterNode.get_node("inventory")
	cameraNode = get_node(cameraPath)
	characterNode.connect("died", self, "_on_player_death")
	#characterNode.get_node("inventory").connect("inventory_changed", self, "_on_inventory_changed")
	characterNode.get_node("inventory").initial_signal_emits()
	set_process(true)
	pass

func _process(delta):
	if characterNode != null:
		character_pos = characterNode.position
	#text.format(0, {"posy"})
	#text.format(1, {"zoom"})
	
	text = ftext.format({"posx": character_pos.x,
						 "posy": character_pos.y,
						 "zoom": 1/cameraNode.zoom.x,
						 "money": characterNode.money,
						 "main_1": get_ui_name("main_1"),
						 "main_2": get_ui_name("main_2"),
						 "sidearm": get_ui_name("sidearm"),
						 "throwable": get_ui_name("throwable"),
						 "tool_1": get_ui_name("tool_1"),
						 "tool_2": get_ui_name("tool_2")})
	pass

func get_ui_name(gun_name):
	if inventoryNode.items[gun_name] == null:
		return "None"
	else:
		return inventoryNode.items[gun_name].ui_name

func _on_player_death():
	set_process(false)
	pass