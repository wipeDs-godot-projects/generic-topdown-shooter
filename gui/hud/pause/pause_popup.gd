extends TextureRect

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("HSeparator/btn_resume").connect("button_down", self, "unpause_game")
	set_process_input(true)
	pass

func _input(event):
	if Input.is_action_just_pressed("pause_game") && get_tree().paused:
		unpause_game()
	pass

func unpause_game():
	hide()
	get_tree().paused = false
	set_process_input(false)
	Utils.get_main_node().set_process_input(true)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
