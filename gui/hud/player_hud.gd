extends Control

export (NodePath) var characterPath

onready var lbl_health = find_node("lbl_health2")
onready var lbl_shield = find_node("lbl_shield2")
onready var lbl_ammo = find_node("lbl_ammo")
onready var lbl_ammo_max = find_node("lbl_max_ammo")
onready var lbl_gun = find_node("lbl_gun")
onready var lbl_zombie_count = find_node("lbl_killcount")
onready var lbl_money = find_node("lbl_money")
onready var lbl_zombie_count_text = find_node("lbl_zombies_left")
onready var lbl_zombies_left = find_node("lbl_zombies_left_count")
onready var reload_bar = find_node("reload_bar")
onready var lbl_enter_shop = find_node("lbl_enter_shop")

var character

func _ready():
	character = get_node(characterPath)
	character.connect("health_changed", self, "_on_player_hit")
	character.connect("shield_changed", self, "_on_shield_changed")
	character.connect("money_changed", self, "_on_player_money_changed")
	character.connect("zombie_killed", self, "_on_player_zombie_killed")
	character.connect("weapon_ammo_count_changed", self, "_on_weapon_ammo_count_changed")
	character.connect("weapon_changed", self, "_on_weapon_changed")
	character.connect("weapon_reload_start", self, "_on_weapon_reload_start")
	character.connect("weapon_reload_finished", self, "_on_weapon_reload_finished")
	character.connect("weapon_reload_abort", self, "_on_weapon_reload_abort")
	character.connect("shop_entered", self, "_on_shop_entered")
	character.connect("shop_exited", self, "_on_shop_exited")
	
	set_zombies_left_texts(Utils.get_main_node().find_node("map").get_child(0))
	_on_player_hit(character.health)
	_on_shield_changed(character.shield)
	_on_weapon_ammo_count_changed(character.current_gun.get_bullets(), character.current_gun.get_ammo_reservoir())
	_on_weapon_changed(character.current_gun)
	pass

func _on_shop_entered(shop):
	lbl_enter_shop.show()

func _on_shop_exited(shop):
	lbl_enter_shop.hide()

func _on_player_hit(hp):
	lbl_health.text = str(hp)
	pass

func _on_shield_changed(hp):
	lbl_shield.text = str(hp)
	pass

func _on_player_money_changed(amount):
	lbl_money.text = str(amount) + "€"
	pass

func _on_player_zombie_killed(count):
	lbl_zombie_count.text = str(count)
	pass

func _on_weapon_ammo_count_changed(ammo, reservoir):
	lbl_ammo.text = str(ammo)
	lbl_ammo_max.text = str(reservoir)

func _on_weapon_changed(gun):
	lbl_gun.text = str(gun.ui_name)
	lbl_ammo.text = str(gun.get_bullets())
	lbl_ammo_max.text = str(gun.get_ammo_reservoir())

func _on_weapon_reload_start(time):
	reload_bar.show()
	lbl_ammo.text = "0"
	reload_bar.reload(time)
	
	print("started reloading" + str(time))

func _on_weapon_reload_finished():
	reload_bar.hide()
	lbl_ammo_max.text = str(character.current_gun.get_ammo_reservoir())
	print("finished reloading")

func _on_weapon_reload_abort():
	reload_bar.abort()
	lbl_ammo.text = str(character.current_gun.bullets)
	print("aborted reloading")

func set_zombies_left_texts(map):
	if map.is_infinite():
		lbl_zombies_left.text = ""
		lbl_zombie_count_text.text = "Infinite Mode"
	else:
		lbl_zombies_left.text = map.zombies_per_round
		lbl_zombie_count_text.text = "Zombies left"