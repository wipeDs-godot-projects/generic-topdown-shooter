extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var crosshair = load("res://assets/gfx/ui/crosshairs/crosshair007.png")

func _ready():
	print(crosshair)
	Input.set_custom_mouse_cursor(crosshair)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
