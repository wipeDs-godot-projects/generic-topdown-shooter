extends Node

const TILESIZE = 64
const SHIELD_PRICE = 5
const RESELL_FACTOR = 0.65

var tilemap
var map_region
var pause_popup
onready var screenshake_percentage = 0


func _ready():
	# Get Backgroundtilemap
	tilemap = Utils.get_main_node().find_node("map").get_child(0).get_child(0).get_node("Background")
	#print(str(tilemap))
	map_region = Vector2(tilemap.get_used_rect().size.x * TILESIZE, tilemap.get_used_rect().size.y * TILESIZE)
	pause_popup = Utils.get_main_node().find_node("pause_popup")
	get_node("Camera").set_bounds()
	get_node("zombiespawner").set_bounds()
	#print(map_region)
	set_process_input(true)
	pass

func _input(event):
	if Input.is_action_just_pressed("pause_game"): pause_game()
	pass

func pause_game():
	get_tree().paused = true
	pause_popup.show()
	pause_popup.set_process_input(true)
	set_process_input(false)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
