extends Node

const GROUP_PLAYER = "grp_player"
const GROUP_ZOMBIE = "grp_zombie"
const GROUP_BULLET = "grp_bullet"
const GROUP_ENTITY = "grp_entity"
const GROUP_PICKUP = "grp_pickup"

func get_main_node():
	var root_node = get_tree().root
	return root_node.get_child(root_node.get_child_count()-1)
	pass