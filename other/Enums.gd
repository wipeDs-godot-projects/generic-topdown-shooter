extends Node

# This script is just a collection of enums used by other scripts!

enum Drops {NONE, AMMO, HEALTH, MONEY}
enum WEAPON_SLOT_TYPE {RIFLE, SIDEARM, THROWABLE, TOOL}