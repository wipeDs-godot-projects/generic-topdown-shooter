extends Camera2D

const DEFAULT_WIDTH = 1280
const DEFAULT_HEIGHT = 720

#onready var target = Utils.get_main_node()
onready var game_node = Utils.get_main_node()
onready var screenshake_timer = Timer.new()
onready var target = get_parent().find_node("Character")
onready var res_zoom_scale = 1

func _ready():
	set_process(true)
	screenshake_timer.wait_time = 2.0
	target.connect("died", self, "_on_target_death")
	get_viewport().connect("size_changed", self, "res_scale_zoom")
	pass

func res_scale_zoom():
	var width = get_viewport().size.x
	var height = get_viewport().size.y
	var coefficient = Vector2(DEFAULT_WIDTH / width, DEFAULT_HEIGHT / height)
	print(coefficient)
	self.zoom = coefficient

func _on_target_death():
	set_process(false)
	pass

func _process(delta):
	position = target.position
	pass

func set_bounds():
	limit_left = 0
	limit_top = 0
	limit_right = game_node.map_region.x
	limit_bottom = game_node.map_region.y
	pass

func screenshake():
	screenshake_timer.start()
	if screenshake_timer.time_left > 0:
		offset = Vector2(rand_range(0,1)*game_node.screenshake_percentage, rand_range(0,1)*game_node.screenshake_percentage)
		pass
	pass

func reset_screenshake():
	offset = Vector2(0,0)
	pass